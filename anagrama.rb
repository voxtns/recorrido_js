/* Ruby */

def anagrama(texto1,texto2)
    return texto1.delete(" " "," ".").downcase.chars.sort.join === texto2.delete(" " "," ".").downcase.chars.sort.join
end